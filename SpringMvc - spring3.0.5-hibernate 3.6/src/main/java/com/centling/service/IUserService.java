package com.centling.service;

import com.centling.model.MUserEntity;

/**
 * Created by delpan.wang on 2014/8/11.
 */
public interface IUserService {
    public MUserEntity getByUsername(String username);

}
