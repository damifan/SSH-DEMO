package com.centling.service;

import com.centling.dao.UserDAO;
import com.centling.model.MUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by delpan.wang on 2014/8/11.
 */
@Service
@Transactional
public class UserService implements IUserService {
    @Autowired
    private UserDAO userDAO;

    @Override
    public MUserEntity getByUsername(String username) {
        String strSql = " FROM MUserEntity u WHERE u.username=?";
        List<MUserEntity> list = (List<MUserEntity>) userDAO.getListByHQL(0, 0, strSql, username);
        if (!list.isEmpty() && list.size() > 0) {
            return list.get(0);
        }
        return null;
    }
}
