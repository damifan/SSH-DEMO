package com.centling.dao;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by delpan.wang on 2014/8/11.
 */
@Repository
public class UserDAO extends BaseDAO {

    @Override
    public List<?> getListByHQL(int nStartRow, int nRowSize, String strHQL, String... strParams) {
        return super.getListByHQL(nStartRow, nRowSize, strHQL, strParams);
    }
}
