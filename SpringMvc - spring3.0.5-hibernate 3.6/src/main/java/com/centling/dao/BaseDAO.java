package com.centling.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by delpan.wang on 2014/8/11.
 */
public class BaseDAO extends HibernateDaoSupport {

    @Resource
    protected SessionFactory sessionFactory;

    /**
     * @param nStartRow 开始行数
     * @param nRowSize  数量
     * @param strHQL    HQL语句
     * @param strParams 参数
     * @return List<?>    返回类型
     * @throws Exception
     * @Description: HQL查询分页
     * @date 2014-7-28 下午3:05:58
     */
    public List<?> getListByHQL(int nStartRow, int nRowSize, String strHQL, String... strParams) {
        //log.debug("HQL：" + strHQL + "\n Params：" + ArrayUtils.toString(strParams));
        Query query = this.sessionFactory.getCurrentSession().createQuery(strHQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }
        if (nRowSize > 0 && nStartRow > -1) {
            query.setFirstResult(nStartRow);
            query.setMaxResults(nRowSize);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        List<?> objList = query.list();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());

        return objList;
    }

    /**
     * @param strHQL    HQL语句
     * @param strParams 参数
     * @return Object    返回类型
     * @throws Exception
     * @Description: 根据HQL语句查询返回Object
     * @date 2014-7-28 下午3:08:47
     */
    public Object getObjectByHQL(String strHQL, String... strParams) {
        //log.debug("HQL：" + strHQL + "\n Params：" + ArrayUtils.toString(strParams));

        Query query = this.sessionFactory.getCurrentSession().createQuery(strHQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        Object objResult = query.uniqueResult();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());

        return objResult;
    }


    /**
     * @param strHQL    HQL语句
     * @param strParams 参数
     * @return boolean    返回类型
     * @throws Exception
     * @Description: 根据HQL语句执行增删改
     * @date 2014-7-28 下午3:10:57
     */
    public boolean doCUDByHQL(String strHQL, String... strParams) {
        //log.debug("HQL：" + strHQL + "\n Params：" + ArrayUtils.toString(strParams));

        Query query = this.sessionFactory.getCurrentSession().createQuery(strHQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        int influenceRowCount = query.executeUpdate();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());

        if (influenceRowCount > 0) {
            return true;
        }
        return false;
    }


    /**
     * @param obj 对象实体
     * @return void    返回类型
     * @throws Exception
     * @Description: 保存或者更新对象
     * @date 2014-7-28 下午3:12:52
     */
    public void doSaveObject(Object obj) {
        //log.debug("Object：" + obj.getClass());
        this.getHibernateTemplate().saveOrUpdate(obj);
    }


    /**
     * @param obj 对象实体
     * @return void    返回类型
     * @Description:保存用户对象
     * @date 2014-6-16 上午10:03:44
     */
    public void doSave(Object obj) {
        this.getHibernateTemplate().save(obj);
    }


    /**
     * @param strSQL    SQL语句
     * @param strParams 参数
     * @return Object    返回类型
     * @throws Exception
     * @Description: 根据SQL语句获得对象
     * @date 2014-7-28 下午3:14:21
     */
    public Object getObjectBySQL(String strSQL, String... strParams) {
        //log.debug("SQL：" + strSQL + "\n Params：" + ArrayUtils.toString(strParams));

        Query query = this.sessionFactory.getCurrentSession().createSQLQuery(strSQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        Object objResult = query.uniqueResult();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());

        return objResult;
    }


    /**
     * @param nStartRow 开始数
     * @param nRowSize  数量
     * @param strSQL    SQL语句
     * @param strParams 参数
     * @return List<?>    返回类型
     * @throws Exception
     * @Description: 根据指定行数查询数据
     * @date 2014-7-28 下午3:15:49
     */
    public List<?> getListBySQL(int nStartRow, int nRowSize, String strSQL, String... strParams) {
        //log.debug("SQL：" + strSQL + "\n Params：" + ArrayUtils.toString(strParams));

        Query query = this.sessionFactory.getCurrentSession().createSQLQuery(strSQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }
        if (nRowSize > 0 && nStartRow > -1) {
            query.setFirstResult(nStartRow);
            query.setMaxResults(nRowSize);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        List<?> objList = query.list();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());

        return objList;
    }


    /**
     * @param strPROCSQL 存储过程
     * @param strParams  参数
     * @return boolean    返回类型
     * @throws Exception
     * @Description: 执行存储过程
     * @date 2014-7-28 下午3:19:13
     */
    public boolean doProcUpdate(String strPROCSQL, String... strParams) {
        //log.debug("PROCSQL：" + strPROCSQL + "\n Params：" + ArrayUtils.toString(strParams));

        Query query = this.sessionFactory.getCurrentSession().createSQLQuery(strPROCSQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        int influenceRowCount = query.executeUpdate();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());

        if (influenceRowCount > 0) {
            return true;
        }
        return false;
    }


    /**
     * @param strPROCSQL SQL语句
     * @param strParams  参数
     * @return List<?>    返回类型
     * @throws Exception
     * @Description: 执行带有返回值的存储过程
     * @date 2014-7-28 下午3:20:26
     */
    public List<?> getProcResult(String strPROCSQL, String... strParams) {
        //log.debug("PROCSQL：" + strPROCSQL + "\n Params：" + ArrayUtils.toString(strParams));

        Query query = this.sessionFactory.getCurrentSession().createSQLQuery(strPROCSQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        List<?> list = query.list();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());

        return list;

    }


    /**
     * @param strPROCSQL SQL语句
     * @param strParams  参数
     * @return void    返回类型
     * @throws Exception
     * @Description: 执行未有参数的存储过程
     * @date 2014-7-28 下午3:21:24
     */
    public void doProcQuery(String strPROCSQL, String... strParams) {
        //log.debug("PROCSQL：" + strPROCSQL + "\n Params：" + ArrayUtils.toString(strParams));

        Query query = this.sessionFactory.getCurrentSession().createSQLQuery(strPROCSQL);
        for (int j = 0; j < strParams.length; j++) {
            query.setString(j, strParams[j]);
        }

        //log.debug("BEGIN：" + DateUtils.getStrOfDateMinute());
        query.list();
        //log.debug("END：" + DateUtils.getStrOfDateMinute());
    }


    /**
     * @return void    返回类型
     * @Description: 清空当前session操作
     * @date 2014-7-28 下午3:21:54
     */
    public void doSessionClear() {
        this.sessionFactory.getCurrentSession().clear();
    }

}
