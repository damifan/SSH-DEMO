package com.centling.controller;

import com.centling.model.MUserEntity;
import com.centling.service.IUserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by delpan.wang on 2014/8/11.
 */
@Controller
public class LoginController {
    private static Logger log = Logger.getLogger(LoginController.class);

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "login")
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response, MUserEntity mUser) {
        log.info("start login...");
        ModelAndView mv = null;
        MUserEntity userEntity = userService.getByUsername(mUser.getUsername());
        log.info("login stop....");
        if (userEntity != null && mUser.getPassword().equals(userEntity.getPassword())) {
            mv = new ModelAndView("/index/index", "mUser", "Login SUCCESS:" + mUser.getUsername());
        } else if (userEntity != null && !mUser.getPassword().equals(userEntity.getPassword())) {
            mv = new ModelAndView("/index/index", "mUser", "Login FAILED:PASSWORD IS WRONG!");
        } else if (userEntity == null) {
            mv = new ModelAndView("/index/index", "mUser", "Login FAILED:USER IS NOT FOUND!");
        }
        return mv;

    }
}
