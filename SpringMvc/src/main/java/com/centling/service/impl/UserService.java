package com.centling.service.impl;

import com.centling.dao.IUserDAO;
import com.centling.model.MUserEntity;
import com.centling.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by delpan.wang on 2014/8/11.
 */
@Service
@Transactional
public class UserService implements IUserService {
    @Autowired
    private IUserDAO userDAO;

    @Override
    public int lookUser() {
        return 0;
    }

    @Override
    public int deleteUser(int id) {
        return 0;
    }

    @Override
    public void saveUser(MUserEntity user) {

    }

    @Override
    public MUserEntity lookUserByUsername(String username) {
        String hql = "from MUserEntity u where u.username=?";
        List<MUserEntity> list = userDAO.findAll(hql, MUserEntity.class, username);
        if (list.size() > 0) {
            return list.get(0);
        }
        return null;
    }
}
