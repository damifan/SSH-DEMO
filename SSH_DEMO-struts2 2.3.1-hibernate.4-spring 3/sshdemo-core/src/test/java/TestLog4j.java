import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

/**
 * Created by Delpan.Wang@centling.com
 * Date: 2014/4/26
 * Time: 22:35.
 */
public class TestLog4j {
    public static void main(String[] args) {
        PropertyConfigurator.configure("E:\\SSH_DEMO\\sshdemo-core\\src\\main\\resources\\config\\log4j.properties");
        Logger logger = Logger.getLogger(TestLog4j.class);
        logger.debug(" debug ");
        logger.error(" error ");
    }
}
