package com.centling.service;

import com.centling.entity.PersonEntity;

/**
 * Created by Delpan.Wang@centling.com
 * Date: 2014/4/24
 * Time: 15:59.
 */
public interface IPersonService {

    public boolean save(PersonEntity personEntity);
}
