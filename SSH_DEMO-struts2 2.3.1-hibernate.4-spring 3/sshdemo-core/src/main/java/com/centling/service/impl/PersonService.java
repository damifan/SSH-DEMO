package com.centling.service.impl;

import com.centling.service.IPersonService;
import com.centling.dao.IPersonDAO;
import com.centling.entity.PersonEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by Delpan.Wang@centling.com
 * Date: 2014/4/24
 * Time: 16:01.
 */
@Service("personService")
@Transactional()
public class PersonService implements IPersonService {

    public IPersonDAO getPersonDAO() {
        return personDAO;
    }

    @Resource(name = "personDAO")
    public void setPersonDAO(IPersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    private IPersonDAO personDAO;

    @Override
    public boolean save(PersonEntity personEntity) {
        System.out.println("-------------from PersonService.processSave()" + personEntity.getUsername() + "personDAO.save(personEntity)" + personDAO.save(personEntity));
        return personDAO.save(personEntity);
    }


}
