package com.centling.dao;

import java.util.List;

/**
 * Created by Delpan.Wang@centling.com
 * Date: 2014/4/24
 * Time: 22:22.
 */
public interface BaseDAO {

    public <T> boolean save(T t);

    public <T> void delete(T t);

    public <T> void delete(Class<T> entityClass, Integer id);

    public <T> void update(T t);

    public <T> T get(Class<T> entityClass, Integer id);

    public <T> List<T> findAll(String hql, Class<T> entityClass);

    public <T> List<T> findAll(String hql, Class<T> entityClass,
                               Object param);

    public <T> List<T> findAll(String hql, Class<T> entityClass,
                               Object[] params);

    public <T> List<T> findByPage(final String hql, final Class<T>
            entityClass, final int firstResult, final int maxResult);

    public <T> List<T> findByPage(final String hql, final Class<T>
            entityClass, final Object param, final int firstResult,
                                  final int maxResult);

    public <T> List<T> findByPage(final String hql, final Class<T>
            entityClass, final Object[] params,
                                  final int firstResult, final int maxResult);

}

