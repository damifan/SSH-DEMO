package com.centling.dao.impl;

import com.centling.dao.IPersonDAO;
import com.centling.entity.PersonEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Delpan.Wang@centling.com
 * Date: 2014/4/24
 * Time: 15:55.
 */
@Repository("personDAO")
public class PersonDAO implements IPersonDAO {

    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public <T> boolean save(T t) {
        System.out.println("------------from PersonDao.save()");
        getSession().save(t);
        return true;
    }

    @Override
    public <T> void delete(T t) {

    }

    @Override
    public <T> void delete(Class<T> entityClass, Integer id) {

    }

    @Override
    public <T> void update(T t) {

    }

    @Override
    public <T> T get(Class<T> entityClass, Integer id) {
        return null;
    }

    @Override
    public <T> List<T> findAll(String hql, Class<T> entityClass) {
        return null;
    }

    @Override
    public <T> List<T> findAll(String hql, Class<T> entityClass, Object param) {
        return null;
    }

    @Override
    public <T> List<T> findAll(String hql, Class<T> entityClass, Object[] params) {
        return null;
    }

    @Override
    public <T> List<T> findByPage(String hql, Class<T> entityClass, int firstResult, int maxResult) {
        return null;
    }

    @Override
    public <T> List<T> findByPage(String hql, Class<T> entityClass, Object param, int firstResult, int maxResult) {
        return null;
    }

    @Override
    public <T> List<T> findByPage(String hql, Class<T> entityClass, Object[] params, int firstResult, int maxResult) {
        return null;
    }
}
