package com.centling.action;


import com.centling.service.IPersonService;
import com.centling.entity.PersonEntity;
import com.centling.util.BaseAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.apache.log4j.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

/**
 * Created by Delpan.Wang@centling.com
 * Date: 2014/4/24
 * Time: 14:29.
 */
@ParentPackage(value = "default")
@Namespace("/person")
@Controller("loginAction")
@Scope(value = "prototype")
public class LoginAction extends BaseAction {
    private Logger log= Logger.getLogger(LoginAction.class);

    private String username;
    private String password;

    private IPersonService personService;

    @Resource(name = "personService")
    public void setPersonService(IPersonService personService) {
        this.personService = personService;
    }

    public IPersonService getPersonService() {
        return personService;
    }

    private PersonEntity personEntity = new PersonEntity();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Action(value = "/tologin", results = {@Result(name = SUCCESS, location = "/login.jsp")})
    public String tologin() {
        return SUCCESS;
    }

    @Action(value = "/login", results = {@Result(name = SUCCESS, location = "/login_success.jsp"), @Result(name = INPUT, location = "/login.jsp")})
    public String login() {
        log.error("username:"+username+"\n"+"password:"+password);
        personEntity.setUsername(username);
        personEntity.setPassword(password);
        if (personService.save(personEntity)) {
            return SUCCESS;
        }
        return INPUT;
    }

    @PostConstruct
    public void init() {
        System.out.println("初始化执行方法");
    }

    @PreDestroy
    public void destory() {
        System.out.println("关闭资源");
    }
}
