package com.centling.entity;

import javax.persistence.*;
import java.io.Serializable;


/**
 * Created by Delpan.Wang@centling.com
 * Date: 2014/4/24
 * Time: 15:14.
 */
@Entity
@Table(name = "PersonEntity")
public class PersonEntity implements Serializable {
    private long id;
    private String username;
    private String password;

    public PersonEntity() {
    }

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(length = 50, nullable = false, unique = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(length = 50, nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
