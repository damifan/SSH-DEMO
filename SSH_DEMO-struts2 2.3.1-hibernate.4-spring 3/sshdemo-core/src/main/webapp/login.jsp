<%--
  Created by IntelliJ IDEA.
  User: centling
  Date: 2014/4/24
  Time: 13:56
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>登录界面</title>
</head>

<body>
<s:form action="person/login.action" method="post">
    <s:textfield name="username" label="username"></s:textfield>
    <s:password name="password" label="password"></s:password>

    <s:submit value="login"></s:submit>
</s:form>
</body>
</html>
