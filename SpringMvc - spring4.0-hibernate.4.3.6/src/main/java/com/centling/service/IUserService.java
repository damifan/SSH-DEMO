package com.centling.service;

import com.centling.model.MUserEntity;

import java.util.List;

/**
 * Created by delpan.wang on 2014/8/11.
 */
public interface IUserService {
    public int lookUser();

    public int deleteUser(int id);

    public void saveUser(MUserEntity user);

    public MUserEntity lookUserByUsername(String username);
}
